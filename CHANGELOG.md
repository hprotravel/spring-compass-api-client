## 0.26.0 (January 13, 2025)
  - Feature - CK-739 - Rename tax related fields

## 0.25.0 (October 28, 2024)
  - Feature - CK-704 - Update bulk endpoints

## 0.24.0 (September 06, 2024)
  - Feature - CK-425 - Set Java version for Jitpack

## 0.23.0 (September 05, 2024)
  - Feature - CK-425 - Upgrade Java to 17 and Spring Boot to 3.0

## 0.22.0 (August 16, 2024)
  - Feature - CK-589 - Move rate option to RatePlan
  - Feature - CK-567 - Read Api validation error responses

## 0.21.0 (June 25, 2024)
  - Feature - CK-597 - Migrate deprecated room rate delete endpoint
  - Feature - CK-597 - Migrate deprecated room rate create endpoint
  - Feature - CK-597 - Migrate deprecated room rate get endpoint

## 0.20.1 (June 07, 2024)
  - Hotfix - CK-612 - Add new fields to Rate Plan and Contract Room

## 0.20.0 (February 19, 2024)
  - Feature - CK-509 - Add dates to Contract

## 0.19.0 (September 06, 2023)
  - Feature - CK-374 - Apply RetryTemplate and specify backoff policy

## 0.18.0 (July 19, 2023)
  - Feature - CK-405 - Replace contractRoomId field

## 0.17.0 (June 15, 2023)
  - Feature - CK-398 - Install hprotravel/java-common-utils package and use StringUtils helper

## 0.16.0 (April 27, 2023)
  - Feature - CK-374 - Add Spring Retry for bulk actions

## 0.15.5 (March 22, 2023)
  - Hotfix - CK-386 - Add contract status field

## 0.15.4 (December 22, 2022)
  - Hotfix - CK-315 - Add timezone fields to booking dates

## 0.15.3 (December 12, 2022)
  - Feature - CK-189 - Add ContractRoomId to AllotmentPlan Room and Restriction

## 0.15.2 (November 28, 2022)
  - Hotfix - CK-314 - Add response error handler support

## 0.15.1 (November 15, 2022)
  - Hotfix - CK-292 - Change model field for missing fields

## 0.15.0 (October 21, 2022)
  - Feature - CK-269 - Add allotment bulk update

## 0.14.0 (October 19, 2022)
  - Set Jitpack JDK to 11

## 0.13.0 (October 18, 2022)
  - Feature - CK-261 - Add request id header for requests

## 0.12.0 (October 14, 2022)
  - Hotfix - CK-266 - Cache most used fetch endpoints for 1 hour

## 0.11.0 (October 11, 2022)
  - Hotfix - CK-254 - Add bulk rate update endpoint

## 0.10.1 (October 10, 2022)
  - Hotfix - CK-256 - Add pax option to rate delete model

## 0.10.0 (July 01, 2022)
  - Improvement - CK-82 - Refactor package name
  - Improvement - CK-103 - Improve README

## 0.9.2 (April 28, 2021)
  - Make request logging optional

## 0.9.1 (March 25, 2021)
  - Fix contract rate plan response

## 0.9.0 (February 22, 2021)
  - Add query to Consumer entity

## 0.8.1 (January 25, 2021)
  - Fix RateContent value
  - Add remark field
  - Add childMinAge field

## 0.8.0 (October 22, 2020)
  - Hotfix - Add generic list template for pagination type responses

## 0.7.0 (October 15, 2020)
  - Add support for more method types and other improvements

## 0.6.0 (January 28, 2020)
  - CP-2914 - Add update rate plans rooms
  - Add get booking with id request

## 0.5.0 (July 11, 2019)
  - Add parent element to Region

## 0.4.0 (July 08, 2019)
  - Add rate plan room rate delete request with pax number

## 0.3.0 (June 14, 2019)
  - Add booking related entities for CP-2607

## 0.2.0 (June 10, 2019)
  - CP-2606 - Add needed fields and methods for user hotelier

## 0.1.0 (May 24, 2019)
  - Add needed fields and methods for CP-2604
  - Add cardexpiredate field
  - Added consumer list service
  - Non-merged commit of old repository
  - Updated pom.xml for jackson-databind security
  - Initial commit
  - Updated gitignore
  - Initial commit

