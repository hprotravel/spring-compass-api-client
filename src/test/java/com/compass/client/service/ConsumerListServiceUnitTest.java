package com.compass.client.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.compass.client.model.Consumer;
import com.compass.client.model.ListResponse;
import com.compass.client.util.HelperEntityService;
import com.compass.client.util.QueryProcessingService;
import com.compass.client.util.ServiceProperties;
import java.util.Collections;
import java.util.HashMap;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ConsumerListServiceUnitTest {

	@Test
	public void getConsumers() {
		ServiceProperties serviceProperties = Mockito.mock(ServiceProperties.class);
		RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
		HelperEntityService helperEntityService = Mockito.mock(HelperEntityService.class);

		ConsumerListService consumerListService = new ConsumerListService(
				serviceProperties, restTemplate, helperEntityService
		);

		HashMap<String, String> headerMap = new HashMap<>();
		headerMap.put("x-consumer-key","1");
		headerMap.put("x-contract-id", "1");

		Consumer consumer = new Consumer();
		consumer.setId(1);
		consumer.setSlug("slug");
		consumer.setConsumerKey("xxxxx");

		ListResponse<Consumer> response = new ListResponse<Consumer>().addElement(consumer);

		ResponseEntity<ListResponse<Consumer>> consumerListResponseEntity = new ResponseEntity<>(
				response, HttpStatus.OK
		);

		when(
			restTemplate.exchange(
				anyString(),
				eq(HttpMethod.GET),
				ArgumentMatchers.<HttpEntity<String>> any(),
				ArgumentMatchers.<ParameterizedTypeReference<ListResponse<Consumer>>> any()
			)
		).thenReturn(consumerListResponseEntity);

		consumerListService.getConsumersList(
				headerMap,
				QueryProcessingService.getQueryParsingervice(Collections.singletonMap("perPage", "1000"))
		);

		verify(restTemplate, times(1)).exchange(
			anyString(),
			eq(HttpMethod.GET),
			ArgumentMatchers.<HttpEntity<String>> any(),
			ArgumentMatchers.<ParameterizedTypeReference<ListResponse<Consumer>>> any()
		);
	}
}
