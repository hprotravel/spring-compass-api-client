package com.compass.client.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.compass.client.model.ContractRoom;
import com.compass.client.util.HelperEntityService;
import com.compass.client.util.ServiceProperties;
import java.util.HashMap;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ContractRoomServiceUnitTest {

    @Test
    public void getContractRoom() {

        ServiceProperties serviceProperties = Mockito.mock(ServiceProperties.class);
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        HelperEntityService helperEntityService = Mockito.mock(HelperEntityService.class);

        ContractRoomService contractRoomService = new ContractRoomService(
                serviceProperties,
                restTemplate ,
                helperEntityService);

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put("x-consumer-key","1");
        headerMap.put("x-contract-id", "1");

        ContractRoom contractRoom = new ContractRoom();
        contractRoom.setName("testRoom");
        contractRoom.setId(1);

        ResponseEntity<ContractRoom> contractRoomResponseEntity = new ResponseEntity<>(contractRoom, HttpStatus.OK);

        when(
            restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                ArgumentMatchers.<HttpEntity<String>> any(),
                eq(ContractRoom.class)
            )
        ).thenReturn(contractRoomResponseEntity);

        contractRoomService.getContractsRooms(headerMap, 1,1);

        verify(restTemplate, times(1)).exchange(
            anyString(),
            eq(HttpMethod.GET),
            ArgumentMatchers.<HttpEntity<String>> any(),
            eq(ContractRoom.class)
        );
    }
}
