package com.compass.client.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.compass.client.model.RatePlanRoomDelete;
import com.compass.client.model.RatePlanRoomUpdate;
import com.compass.client.util.HeaderService;
import com.compass.client.util.HelperEntityService;
import com.compass.client.util.ServiceProperties;
import java.util.Collections;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

public class RatePlanRoomServiceUnitTest {

  @Test
  public void updateRatePlansRoomsBulk() {
    ServiceProperties serviceProperties = Mockito.mock(ServiceProperties.class);
    RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
    HelperEntityService helperEntityService = Mockito.mock(HelperEntityService.class);
    HeaderService headerService = Mockito.mock(HeaderService.class);

    RatePlanRoomService service = new RatePlanRoomService(
        serviceProperties, restTemplate, headerService, helperEntityService
    );

    ResponseEntity<String> responseEntity = new ResponseEntity<>("", HttpStatus.OK);

    when(
        restTemplate.exchange(
            anyString(),
            eq(HttpMethod.POST),
            ArgumentMatchers.<HttpEntity<String>> any(),
            eq(String.class)
        )
    )
        .thenThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR))
        .thenThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR))
        .thenReturn(responseEntity);

    service.bulkUpdate(
        Collections.singletonMap("foo", "bar"),
        999,
        Collections.singletonList(new RatePlanRoomUpdate())
    );

    verify(restTemplate, times(3)).exchange(
        anyString(),
        eq(HttpMethod.POST),
        ArgumentMatchers.<HttpEntity<String>> any(),
        eq(String.class)
    );
  }

  @Test
  public void deleteRatePlansRoomsBulk() {
    ServiceProperties serviceProperties = Mockito.mock(ServiceProperties.class);
    RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
    HelperEntityService helperEntityService = Mockito.mock(HelperEntityService.class);
    HeaderService headerService = Mockito.mock(HeaderService.class);

    RatePlanRoomService service = new RatePlanRoomService(
        serviceProperties, restTemplate, headerService, helperEntityService
    );

    ResponseEntity<String> responseEntity = new ResponseEntity<>("", HttpStatus.OK);

    when(
        restTemplate.exchange(
            anyString(),
            eq(HttpMethod.DELETE),
            ArgumentMatchers.<HttpEntity<String>> any(),
            eq(String.class)
        )
    )
        .thenThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR))
        .thenThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR))
        .thenReturn(responseEntity);

    service.bulkDelete(
        Collections.singletonMap("foo", "bar"),
        999,
        Collections.singletonList(new RatePlanRoomDelete())
    );

    verify(restTemplate, times(3)).exchange(
        anyString(),
        eq(HttpMethod.DELETE),
        ArgumentMatchers.<HttpEntity<String>> any(),
        eq(String.class)
    );
  }
}
