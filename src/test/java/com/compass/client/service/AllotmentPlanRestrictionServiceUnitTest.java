package com.compass.client.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.compass.client.model.AllotmentPlanRestriction;
import com.compass.client.model.AllotmentPlanRestrictionUpdate;
import com.compass.client.util.HeaderService;
import com.compass.client.util.HelperEntityService;
import com.compass.client.util.ServiceProperties;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

public class AllotmentPlanRestrictionServiceUnitTest {

    @Test
    public void getAllotmentPlansRestrictionsList() {

        ServiceProperties serviceProperties = Mockito.mock(ServiceProperties.class);
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        HelperEntityService helperEntityService = Mockito.mock(HelperEntityService.class);
        HeaderService headerService = Mockito.mock(HeaderService.class);

        AllotmentPlanRestrictionService allotmentPlanRestrictionService = new AllotmentPlanRestrictionService(
                serviceProperties, restTemplate, helperEntityService, headerService
        );

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put("x-consumer-key","1");
        headerMap.put("x-contract-id", "1");

        List<AllotmentPlanRestriction> allotmentPlanRestrictionList = new ArrayList<>();
        AllotmentPlanRestriction allotmentPlanRestriction = new AllotmentPlanRestriction();
        allotmentPlanRestriction.setId(1);
        allotmentPlanRestrictionList.add(allotmentPlanRestriction);
        ResponseEntity<List<AllotmentPlanRestriction>> allotmentPlanRestrictionResponseEntity =
                new ResponseEntity<>(allotmentPlanRestrictionList, HttpStatus.OK);

        when(
            restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                ArgumentMatchers.<HttpEntity<String>> any(),
                ArgumentMatchers.<ParameterizedTypeReference<List<AllotmentPlanRestriction>>> any()
            )
        ).thenReturn(allotmentPlanRestrictionResponseEntity);

        allotmentPlanRestrictionService.
                getAllotmentPlansRestrictionsList(
                        headerMap,1, "?fromDate=2017-10-10&toDate=2018-10-10");

        verify(restTemplate, times(1))
            .exchange(
                anyString(),
                eq(HttpMethod.GET),
                ArgumentMatchers.<HttpEntity<String>>any(),
                ArgumentMatchers.<ParameterizedTypeReference<List<AllotmentPlanRestriction>>>any()
            );
    }

    @Test
    public void updateAllotmentPlansRestrictionsBulk() {
        ServiceProperties serviceProperties = Mockito.mock(ServiceProperties.class);
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        HelperEntityService helperEntityService = Mockito.mock(HelperEntityService.class);
        HeaderService headerService = Mockito.mock(HeaderService.class);

        AllotmentPlanRestrictionService service = new AllotmentPlanRestrictionService(
            serviceProperties, restTemplate,  helperEntityService, headerService
        );

        ResponseEntity<String> responseEntity = new ResponseEntity<>("", HttpStatus.OK);

        when(restTemplate.exchange(
            anyString(),
            eq(HttpMethod.POST),
            ArgumentMatchers.<HttpEntity<String>>any(),
            eq(String.class)
        ))
            .thenThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR))
            .thenThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR))
            .thenReturn(responseEntity);

        service.bulkUpdate(
            Collections.singletonMap("foo", "bar"),
            999,
            Collections.singletonList(new AllotmentPlanRestrictionUpdate())
        );

        verify(restTemplate, times(3)).exchange(
            anyString(),
            eq(HttpMethod.POST),
            ArgumentMatchers.<HttpEntity<String>>any(),
            eq(String.class)
        );
    }

    @Test
    public void deleteAllotmentPlansRestrictionsBulk() {
        ServiceProperties serviceProperties = Mockito.mock(ServiceProperties.class);
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        HelperEntityService helperEntityService = Mockito.mock(HelperEntityService.class);
        HeaderService headerService = Mockito.mock(HeaderService.class);

        AllotmentPlanRestrictionService service = new AllotmentPlanRestrictionService(
            serviceProperties, restTemplate, helperEntityService, headerService
        );

        ResponseEntity<String> responseEntity = new ResponseEntity<>("", HttpStatus.OK);

        when(
            restTemplate.exchange(
                anyString(),
                eq(HttpMethod.DELETE),
                ArgumentMatchers.<HttpEntity<String>>any(),
                eq(String.class)
            )
        )
            .thenThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR))
            .thenThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR))
            .thenReturn(responseEntity);

        service.bulkDelete(
            Collections.singletonMap("foo", "bar"),
            999,
            Collections.singletonList(new AllotmentPlanRestrictionUpdate())
        );

        verify(restTemplate, times(3))
            .exchange(
                anyString(),
                eq(HttpMethod.DELETE),
                ArgumentMatchers.<HttpEntity<String>>any(),
                eq(String.class)
        );
    }
}
