package com.compass.client;

import com.compass.client.model.AllotmentPlanRestriction;
import com.compass.client.model.AllotmentPlanRestrictionUpdate;
import com.compass.client.model.AllotmentPlanRoom;
import com.compass.client.model.AllotmentPlanRoomUpdate;
import com.compass.client.model.Consumer;
import com.compass.client.model.Contract;
import com.compass.client.model.ContractInventoryPlan;
import com.compass.client.model.ContractList;
import com.compass.client.model.ContractRatePlan;
import com.compass.client.model.ContractRoom;
import com.compass.client.model.RatePlan;
import com.compass.client.model.RatePlanCancelPolicy;
import com.compass.client.model.RatePlanCancelPolicyUpdate;
import com.compass.client.model.RatePlanRoomDelete;
import com.compass.client.model.RatePlanRoomRate;
import com.compass.client.model.RatePlanRoomUpdate;
import com.compass.client.model.RatePlanSaleChannel;
import com.compass.client.model.RateUpdate;
import com.compass.client.model.User;
import com.compass.client.model.booking.Booking;
import com.compass.client.model.booking.ProductBooking;
import com.compass.client.model.booking.paymentinfo.PaymentInfo;
import com.compass.client.service.AllotmentPlanRestrictionService;
import com.compass.client.service.AllotmentPlanRoomService;
import com.compass.client.service.ConsumerListService;
import com.compass.client.service.ContractInventoryPlanService;
import com.compass.client.service.ContractListService;
import com.compass.client.service.ContractRatePlanService;
import com.compass.client.service.ContractRoomListService;
import com.compass.client.service.ContractRoomService;
import com.compass.client.service.ContractService;
import com.compass.client.service.ProductBookingService;
import com.compass.client.service.RatePlanCancelPolicyService;
import com.compass.client.service.RatePlanRoomListService;
import com.compass.client.service.RatePlanRoomService;
import com.compass.client.service.RatePlanSaleChannelListService;
import com.compass.client.service.RatePlanService;
import com.compass.client.service.UserService;
import com.compass.client.util.HeaderService;
import com.compass.client.util.HelperEntityService;
import com.compass.client.util.LoggingRequestInterceptor;
import com.compass.client.util.QueryProcessingService;
import com.compass.client.util.ServiceProperties;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

public class CompassClientImpl implements CompassClient {

    private RestTemplate restTemplate;
    private ContractRatePlanService contractRatePlanService;
    private AllotmentPlanRoomService allotmentPlanRoomService;
    private AllotmentPlanRestrictionService allotmentPlanRestrictionService;
    private ServiceProperties serviceProperties;
    private ContractInventoryPlanService contractInventoryPlanService;
    private ContractRoomListService contractRoomListService;
    private ContractListService contractListService;
    private ContractService contractService;
    private RatePlanService ratePlanService;
    private ContractRoomService contractRoomService;
    private ProductBookingService productBookingService;
    private RatePlanRoomListService ratePlanRoomListService;
    private RatePlanRoomService ratePlanRoomService;
    private RatePlanCancelPolicyService ratePlanCancelPolicyService;
    private RatePlanSaleChannelListService ratePlanSaleChannelListService;
    private UserService userService;
    private ConsumerListService consumerListService;
    private Cache<Object, Object> cache;

    public void setRatePlanRoomService(RatePlanRoomService ratePlanRoomService) {
        this.ratePlanRoomService = ratePlanRoomService;
    }

    public void setProductBookingService(ProductBookingService productBookingService) {
        this.productBookingService = productBookingService;
    }

    public void setContractRoomService(ContractRoomService contractRoomService) {
        this.contractRoomService = contractRoomService;
    }

    public void setRatePlanService(RatePlanService ratePlanService) {
        this.ratePlanService = ratePlanService;
    }

    public void setContractService(ContractService contractService) {
        this.contractService = contractService;
    }

    public void setContractInventoryPlanService(ContractInventoryPlanService contractInventoryPlanService) {
        this.contractInventoryPlanService = contractInventoryPlanService;
    }

    public void setContractRatePlanService(ContractRatePlanService contractRatePlanService) {
        this.contractRatePlanService = contractRatePlanService;
    }

    public void setAllotmentPlanRoomService(AllotmentPlanRoomService allotmentPlanRoomService) {
        this.allotmentPlanRoomService = allotmentPlanRoomService;
    }

    public void setAllotmentPlanRestrictionService(AllotmentPlanRestrictionService allotmentPlanRestrictionService) {
        this.allotmentPlanRestrictionService = allotmentPlanRestrictionService;
    }

    public void setContractRoomListService(ContractRoomListService contractRoomListService) {
        this.contractRoomListService = contractRoomListService;
    }

    public void setRatePlanRoomListService(RatePlanRoomListService ratePlanRoomListService) {
        this.ratePlanRoomListService = ratePlanRoomListService;
    }

    public void setRatePlanCancelPolicyService(RatePlanCancelPolicyService ratePlanCancelPolicyService) {
        this.ratePlanCancelPolicyService = ratePlanCancelPolicyService;
    }

    public void setRatePlanSaleChannelListService(RatePlanSaleChannelListService ratePlanSaleChannelListService) {
        this.ratePlanSaleChannelListService = ratePlanSaleChannelListService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setConsumerListService(ConsumerListService consumerListService) {
        this.consumerListService = consumerListService;
    }

    public void setResponseErrorHandler(ResponseErrorHandler errorHandler) {
        this.restTemplate.setErrorHandler(errorHandler);
    }

    public CompassClientImpl(ServiceProperties serviceProperties) {
        this.serviceProperties = serviceProperties;

        cache = Caffeine.newBuilder()
                .expireAfterAccess(10, TimeUnit.MINUTES)
                .build();

        restTemplate = new RestTemplate(
            new BufferingClientHttpRequestFactory(new HttpComponentsClientHttpRequestFactory())
        );
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        List<ClientHttpRequestInterceptor> interceptors;
        interceptors = new ArrayList<>();
        interceptors.add(new LoggingRequestInterceptor(serviceProperties.isRequestLogging()));
        restTemplate.setInterceptors(interceptors);

        contractRatePlanService = new ContractRatePlanService(
                serviceProperties,
                restTemplate,
                new HelperEntityService());

        allotmentPlanRoomService = new AllotmentPlanRoomService(
                serviceProperties,
                restTemplate,
                new HeaderService(),
                new HelperEntityService());

        allotmentPlanRestrictionService = new AllotmentPlanRestrictionService(
                serviceProperties,
                restTemplate,
                new HelperEntityService(),
                new HeaderService());

        contractInventoryPlanService = new ContractInventoryPlanService(
                serviceProperties,
                restTemplate,
                new HelperEntityService());

        contractRoomListService = new ContractRoomListService(
                serviceProperties,
                restTemplate,
                new HelperEntityService());

        contractRoomListService = new ContractRoomListService(
                serviceProperties,
                restTemplate,
                new HelperEntityService());

        contractService = new ContractService(
                serviceProperties,
                restTemplate,
                new HelperEntityService());

        ratePlanService = new RatePlanService(
                serviceProperties,
                restTemplate,
                new HelperEntityService());

        contractRoomService = new ContractRoomService(
                serviceProperties,
                restTemplate,
                new HelperEntityService());

        contractListService = new ContractListService(
                serviceProperties,
                restTemplate,
                new HelperEntityService()
        );

        productBookingService = new ProductBookingService(
                serviceProperties,
                restTemplate,
                new HelperEntityService());

        ratePlanRoomListService = new RatePlanRoomListService(serviceProperties,
                restTemplate, new HeaderService(), new HelperEntityService());

        ratePlanRoomService = new RatePlanRoomService(
                serviceProperties, restTemplate, new HeaderService(), new HelperEntityService());

        ratePlanCancelPolicyService = new RatePlanCancelPolicyService(serviceProperties, restTemplate,
                new HelperEntityService(), new HeaderService());

        ratePlanSaleChannelListService = new RatePlanSaleChannelListService(serviceProperties, restTemplate,
                new HelperEntityService());

        userService = new UserService(serviceProperties, restTemplate, new HelperEntityService());

        consumerListService = new ConsumerListService(
                serviceProperties, restTemplate, new HelperEntityService()
        );
    }

    @Override
    public List<ContractRatePlan> getContractsRatePlans(
        HashMap<String, String> headerMap, Integer contractId
    ) {
        String key = getRequestCacheKey("/contracts/" + contractId + "/rate-plans", headerMap);

        List<ContractRatePlan> contractRatePlans = (List<ContractRatePlan>) cache.getIfPresent(key);
        if (contractRatePlans == null) {
            contractRatePlans = contractRatePlanService.getContractsRatePlans(headerMap, contractId, "");

            cache.put(key, contractRatePlans);
        }

        return contractRatePlans;
    }

    @Override
    public List<AllotmentPlanRestriction> getAllotmentPlansRestrictionsListWithQuery(
            HashMap<String, String> headerMap, Integer allotmentPlanId, HashMap<String, String> queryMap) {

        String query = QueryProcessingService.getQueryParsingervice(queryMap);

        return allotmentPlanRestrictionService.
                getAllotmentPlansRestrictionsList(headerMap, allotmentPlanId, query);
    }

    @Override
    public void createAllotmentPlansRestrictions(
            HashMap<String, String> headerMap,
            Integer allotmentPlanId,
            AllotmentPlanRestrictionUpdate allotmentPlanRestrictionUpdate) {

        allotmentPlanRestrictionService.createAllotmentPlansRestrictions(
                headerMap,
                allotmentPlanId,
                allotmentPlanRestrictionUpdate);
    }

    @Override
    public void bulkUpdateAllotmentPlansRestrictions(
        HashMap<String, String> headerMap,
        int allotmentPlanId,
        List<AllotmentPlanRestrictionUpdate> bulkUpdateList
    ) {
        allotmentPlanRestrictionService.bulkUpdate(
            headerMap, allotmentPlanId, bulkUpdateList
        );
    }

    @Override
    public void deleteAllotmentPlansRestrictions(
            HashMap<String, String> headerMap,
            Integer allotmentPlanId,
            AllotmentPlanRestrictionUpdate allotmentPlanRestrictionUpdate) {

        allotmentPlanRestrictionService.
            deleteAllotmentPlansRestrictions(headerMap, allotmentPlanId, allotmentPlanRestrictionUpdate);
    }

    @Override
    public void bulkDeleteAllotmentPlansRestrictions(
        HashMap<String, String> headerMap,
        int allotmentPlanId,
        List<AllotmentPlanRestrictionUpdate> bulkDeleteList
    ) {
        allotmentPlanRestrictionService.bulkDelete(
            headerMap, allotmentPlanId, bulkDeleteList
        );
    }

    @Override
    public List<AllotmentPlanRestriction> getAllotmentPlansRestrictionsListWithQueryMultiRooms(
            HashMap<String, String> headerMap, Integer allotmentPlanId,
            HashMap<String, String> queryMap, List<Integer> contractRoomIds) {

        String query = QueryProcessingService.getQueryParsingervice(queryMap);

        return allotmentPlanRestrictionService.getAllotmentPlansRestrictionsMultiRooms(headerMap, allotmentPlanId,
                query, contractRoomIds);
    }

    @Override
    public List<AllotmentPlanRoom> getAllotmentPlansRoomsListWithQuery(HashMap<String, String> headerMap,
                                                                       Integer allotmentPlanId,
                                                                       HashMap<String, String> queryMap) {
        String query = QueryProcessingService.getQueryParsingervice(queryMap);

        return allotmentPlanRoomService.getAllotmentPlansRoomsList(headerMap, allotmentPlanId, query);
    }

    @Override
    public List<AllotmentPlanRoom> getAllotmentPlansRoomsListWithQueryMultiRooms(HashMap<String, String> headerMap,
                                                                       Integer allotmentPlanId,
                                                                       HashMap<String, String> queryMap,
                                                                                 List<Integer> contractRoomIds) {
        String query = QueryProcessingService.getQueryParsingervice(queryMap);

        return allotmentPlanRoomService.getAllotmentPlansRoomsListMultiRooms(headerMap, allotmentPlanId, query, contractRoomIds);
    }

    @Override
    public void createAllotmentPlansRooms(HashMap<String, String> headerMap,
                                          Integer allotmentPlanId,
                                          AllotmentPlanRoomUpdate allotmentPlanRoomUpdate) {
        this.changeAllotmentPlansRooms(headerMap, allotmentPlanId, allotmentPlanRoomUpdate, HttpMethod.POST);
    }

    @Override
    public void changeAllotmentPlansRooms(
        HashMap<String, String> headerMap,
        Integer allotmentPlanId,
        AllotmentPlanRoomUpdate allotmentPlanRoomUpdate,
        HttpMethod requestType
    ) {
        allotmentPlanRoomService.changeAllotmentPlansRooms(
            headerMap, allotmentPlanId, allotmentPlanRoomUpdate, requestType
        );
    }

    @Override
    public void bulkUpdateAllotmentPlansRooms(
        HashMap<String, String> headerMap,
        int allotmentPlanId,
        List<AllotmentPlanRoomUpdate> bulkUpdateList
    ) {
        allotmentPlanRoomService.bulkUpdate(headerMap, allotmentPlanId, bulkUpdateList);
    }

    @Override
    public void bulkDeleteAllotmentPlansRooms(
        HashMap<String, String> headerMap,
        int allotmentPlanId,
        List<AllotmentPlanRoomUpdate> bulkDeleteList
    ) {
        allotmentPlanRoomService.bulkDelete(headerMap, allotmentPlanId, bulkDeleteList);
    }

    @Override
    public ContractInventoryPlan getContractsInventoryPlans(HashMap<String,String> headerMap, Integer contractId) {

        return contractInventoryPlanService.getContractsInventoryPlans(headerMap, contractId);
    }

    @Override
    public List<ContractRatePlan> getContractsRatePlansWithQuery(
        HashMap<String,String> headerMap, Integer contractId, HashMap<String, String> queryMap
    ) {
        String query = QueryProcessingService.getQueryParsingervice(queryMap);

        String key = getRequestCacheKey("/contracts/" + contractId + "/rate-plans", headerMap, queryMap);

        List<ContractRatePlan> contractRatePlans = (List<ContractRatePlan>) cache.getIfPresent(key);
        if (contractRatePlans == null) {
            contractRatePlans = contractRatePlanService.getContractsRatePlans(
                headerMap, contractId, query
            );

            cache.put(key, contractRatePlans);
        }

        return contractRatePlans;
    }

    @Override
    public List<ContractRoom> getContractsRoomsList(
        HashMap<String,String> headerMap, Integer contractId
    ) {
        String key = getRequestCacheKey("/contracts/" + contractId + "/rooms", headerMap);

        List<ContractRoom> contractRooms = (List<ContractRoom>) cache.getIfPresent(key);
        if (contractRooms == null) {
            contractRooms = contractRoomListService.getContractsRoomsList(headerMap, contractId, "");

            cache.put(key, contractRooms);
        }

        return contractRooms;
    }

    @Override // TODO: Merge this method with main method.
    public List<ContractRoom> getContractsRoomsListWithQuery(
        HashMap<String,String> headerMap, Integer contractId, HashMap<String, String> queryMap
    ) {
        String query = QueryProcessingService.getQueryParsingervice(queryMap);

        String key = getRequestCacheKey("/contracts/" + contractId + "/rooms", headerMap, queryMap);

        List<ContractRoom> contractRooms = (List<ContractRoom>) cache.getIfPresent(key);
        if (contractRooms == null) {
            contractRooms = contractRoomListService.getContractsRoomsList(
                headerMap, contractId, query
            );

            cache.put(key, contractRooms);
        }

        return contractRooms;
    }

    @Override
    public Contract getContracts(HashMap<String,String> headerMap, Integer contractId) {
        String key = getRequestCacheKey("/contracts/" + contractId, headerMap);

        Contract contract = (Contract) cache.getIfPresent(key);
        if (contract == null) {
            contract = contractService.getContracts(headerMap, contractId, "");

            cache.put(key, contract);
        }

        return contract;
    }

    @Override // TODO: Merge this method with main method.
    public Contract getContractsWithQuery(
        HashMap<String,String> headerMap, Integer contractId, HashMap<String, String> queryMap
    ) {
        String query = QueryProcessingService.getQueryParsingervice(queryMap);

        String key = getRequestCacheKey("/contracts/" + contractId, headerMap, queryMap);

        Contract contract = (Contract) cache.getIfPresent(key);
        if (contract == null) {
            contract = contractService.getContracts(headerMap, contractId, query);

            cache.put(key, contract);
        }

        return contract;
    }

    @Override
    public ContractList getContractsList(HashMap<String,String> headerMap) {

        return contractListService.getContractsList(headerMap);
    }

    @Override
    public RatePlan getRatePlans(HashMap<String,String> headerMap, Integer ratePlanCode, HashMap<String, String> queryMap) {

        String query = QueryProcessingService.getQueryParsingervice(queryMap);

        String key = getRequestCacheKey("/rate-plans/" + ratePlanCode, headerMap, queryMap);

        RatePlan ratePlan = (RatePlan) cache.getIfPresent(key);
        if (ratePlan == null) {
            ratePlan = ratePlanService.getRatePlans(headerMap, ratePlanCode, query);
            cache.put(key, ratePlan);

        }
        return ratePlan;
    }

    @Override
    public ContractRoom getContractsRooms(HashMap<String,String> headerMap, Integer contractId, Integer roomCode) {

        String key = getRequestCacheKey("/contracts/" + contractId + "/rooms/" + roomCode, headerMap);

        ContractRoom contractRoom = (ContractRoom) cache.getIfPresent(key);

        if (contractRoom == null) {
            contractRoom = contractRoomService.getContractsRooms(headerMap, contractId, roomCode);
            cache.put(key, contractRoom);
        }
        return contractRoom;
    }

    @Override
    public ProductBooking getProductsBookings(HashMap<String,String> headerMap) {

        return productBookingService.getProductsBookings(headerMap, "");
    }

    @Override
    public ProductBooking getProductsBookingsWithQuery(HashMap<String,String> headerMap,
                                                       HashMap<String, String> queryMap) {

        String query = QueryProcessingService.getQueryParsingervice(queryMap);

        return productBookingService.getProductsBookings(headerMap, query);
    }

    @Override
    public Booking getProductsBookingsWithId(
        HashMap<String, String> headerMap, Integer bookingId, HashMap<String, String> queryMap
    ) {
        String query = QueryProcessingService.getQueryParsingervice(queryMap);

        return productBookingService.getProductsBookingsWithId(headerMap, bookingId, query);
    }

    @Override
    public PaymentInfo getProductsBookingsPaymentInfo(HashMap<String,String> headerMap,
                                                      Integer bookingId) {

        return productBookingService.getProductsBookingsPaymentInfo(headerMap, bookingId);
    }

    @Override
    public List<RatePlanRoomRate> getRatePlansRooms(
        HashMap<String,String> headerMap,
        Integer ratePlanId,
        HashMap<String, String> queryMap
    ) {
        return ratePlanRoomListService.getRatePlansRooms(
            headerMap, ratePlanId, QueryProcessingService.getQueryParsingervice(queryMap)
        );
    }

    @Override
    public void updateRatePlansRooms(
        HashMap<String, String> headerMap,
        Integer ratePlanId,
        Integer roomId,
        RatePlanRoomUpdate ratePlanRoomUpdate
    ) {
        ratePlanRoomService.updateRatePlansRooms(headerMap, ratePlanId, roomId, ratePlanRoomUpdate);
    }

    @Override
    public void bulkUpdateRatePlansRooms(
        HashMap<String, String> headerMap,
        int ratePlanId,
        List<RatePlanRoomUpdate> bulkUpdateList
    ) {
        ratePlanRoomService.bulkUpdate(headerMap, ratePlanId, bulkUpdateList);
    }

    @Override
    public void deleteRatePlansRooms(HashMap<String,String> headerMap, Integer ratePlanId, RateUpdate rateUpdate) {
        ratePlanRoomListService.deleteRatePlansRooms(headerMap, ratePlanId, rateUpdate);
    }

    @Override
    public void bulkDeleteRatePlansRooms(
        HashMap<String, String> headerMap,
        int ratePlanId,
        List<RatePlanRoomDelete> bulkDeleteList
    ) {
        ratePlanRoomService.bulkDelete(headerMap, ratePlanId, bulkDeleteList);
    }

    @Override
    public List<RatePlanCancelPolicy> getRatePlanCancelPoliciesWithQuery(HashMap<String,String> headerMap,
                                                                         Integer ratePlanId,
                                                                         HashMap<String, String> queryMap) {
        String query = QueryProcessingService.getQueryParsingervice(queryMap);
        return ratePlanCancelPolicyService.getRatePlansCancelPolicies(headerMap, ratePlanId, query);
    }

    @Override
    public void createRatePlansCancelPolicies(HashMap<String,String> headerMap, Integer ratePlanId,
                                              RatePlanCancelPolicyUpdate params) {
        ratePlanCancelPolicyService.createRatePlansCancelPolicies(headerMap, ratePlanId, params);
    }

    @Override
    public List<RatePlanCancelPolicy> getRatePlanCancelPoliciesWithQueryMultiRooms(HashMap<String, String> headerMap,
                                                                                   Integer ratePlanId,
                                                                                   HashMap<String, String> queryMap,
                                                                                   List<Integer> contractRoomIds) {

        String query = QueryProcessingService.getQueryParsingervice(queryMap);

        return ratePlanCancelPolicyService.getRatePlansCancelPoliciesMultiRooms(headerMap,
                ratePlanId, query, contractRoomIds);
    }

    @Override
    public void createRatePlansRooms(HashMap<String,String> headerMap, Integer ratePlanId, RateUpdate rateUpdate) {
        ratePlanRoomListService.createRatePlansRooms(headerMap, ratePlanId, rateUpdate);
    }

    @Override
    public List<RatePlanSaleChannel> getRatePlansSaleChannelsList(HashMap<String, String> headerMap, Integer ratePlanId) {
        return ratePlanSaleChannelListService.getRatePlansSaleChannelsList(headerMap, ratePlanId);
    }

    @Override
    public User getUsers(HashMap<String, String> headerMap, Integer userId, HashMap<String, String> queryMap) {
        String query = QueryProcessingService.getQueryParsingervice(queryMap);
        String key = getRequestCacheKey("/users/" + userId, headerMap, queryMap);

        User user = (User) cache.getIfPresent(key);
        if (user == null) {
            user = userService.getUsers(headerMap, userId, query);
            cache.put(key, user);
        }

        return user;
    }

    @Override
    public List<Consumer> getConsumersList(
        HashMap<String, String> headerMap, HashMap<String, String> queryMap
    ) {
        return consumerListService.getConsumersList(
            headerMap, QueryProcessingService.getQueryParsingervice(queryMap)
        );
    }

    private String getRequestCacheKey(String url, HashMap<String, String> header) {
        return getRequestCacheKey(url, header, new HashMap<>());
    }

    private String getRequestCacheKey(String url, HashMap<String, String> header, HashMap<String, String> query) {
        return DigestUtils.md5Hex(url + header + query);
    }
}
