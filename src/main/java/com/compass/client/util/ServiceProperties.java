package com.compass.client.util;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "compass")
public class ServiceProperties {

    private String url;
    private boolean requestLogging;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isRequestLogging() {
        return requestLogging;
    }

    public void setRequestLogging(boolean requestLogging) {
        this.requestLogging = requestLogging;
    }
}
