package com.compass.client.util;

import java.util.Collections;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

public class RetryUtils {

    private static final int MAX_CLIENT_ATTEMPTS = 3;

    public static void retryBulkRequest(
        String url, HttpMethod method, HttpEntity headerEntity, RestTemplate restTemplate
    ) {
        createRetryTemplate().execute(
            context -> restTemplate.exchange(url, method, headerEntity, String.class)
        );
    }

    private static RetryTemplate createRetryTemplate() {
        RetryTemplate retryTemplate = new RetryTemplate();

        retryTemplate.setRetryPolicy(
            new SimpleRetryPolicy(
                MAX_CLIENT_ATTEMPTS, Collections.singletonMap(HttpServerErrorException.class, true)
            )
        );
        retryTemplate.setBackOffPolicy(new ExponentialBackOffPolicy());

        return retryTemplate;
    }
}
