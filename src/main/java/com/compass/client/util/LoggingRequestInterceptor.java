package com.compass.client.util;

import com.compass.commonutils.StringUtils;
import com.compass.commonutils.StringUtils.Level;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class LoggingRequestInterceptor implements ClientHttpRequestInterceptor{

    private static final String HEADER_REQUEST_ID = "X-Request-Id";
    private static final int HEADER_REQUEST_ID_LENGTH = 16;

    private boolean requestLogging;

    public LoggingRequestInterceptor(boolean requestLogging) {
        this.requestLogging = requestLogging;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {
        request.getHeaders().add(HEADER_REQUEST_ID, this.getTransactionIdentifier());

        ClientHttpResponse response = execution.execute(request, body);

        if (requestLogging) {
            this.addLog(request, body, response);
        }

        return response;
    }

    private void addLog(HttpRequest request, byte[] body, ClientHttpResponse response) {
        String requestContent = new String(body).replace("\n", "");
        String logPayload = "Uri: " + request.getURI()
            + " (" + request.getMethod().name() + ") "
            + " Request: " + requestContent;

        // Bind all headers to log payload
        HttpHeaders headers = request.getHeaders();

        logPayload += " Headers: (" + headers.entrySet().size() + ")";
        for (Map.Entry<String, List<String>> header: headers.entrySet()) {
            logPayload += " " + header.getKey() + ":" +header.getValue().toString();
        }

        String responseContent = "";
        String responseStatus = "";
        Scanner s;
        try {
            s = new Scanner(response.getBody()).useDelimiter("\\A");
            responseContent = s.hasNext() ? s.next() : "";
            responseContent = responseContent.replace("\n", "");

            responseStatus = response.getStatusText();

        } catch (IOException ex) {
            // Skip trying to add response body or status
        }

        logPayload += " ResponseContent: " + responseContent;
        logPayload += " ResponseStatus: " + responseStatus;

        System.out.println(logPayload);
    }

    private String getTransactionIdentifier() {
        return StringUtils.random(HEADER_REQUEST_ID_LENGTH, Level.LOWER);
    }
}
