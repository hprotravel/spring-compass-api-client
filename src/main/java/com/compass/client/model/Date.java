package com.compass.client.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Date {

    public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSSSSS";

    private String date;
    private String timezone;

    @JsonProperty("timezone_type")
    private Integer timezoneType;

    public String getDate() {
        return date;
    }

    public Date setDate(String date) {
        this.date = date;

        return this;
    }

    public String getTimezone() {
        return timezone;
    }

    public Date setTimezone(String timezone) {
        this.timezone = timezone;

        return this;
    }

    public Integer getTimezoneType() {
        return timezoneType;
    }

    public Date setTimezoneType(Integer timezoneType) {
        this.timezoneType = timezoneType;

        return this;
    }

    public LocalDateTime getParsedDateTime() {
        return LocalDateTime
            .parse(this.date, DateTimeFormatter.ofPattern(DATE_PATTERN))
            .atZone(ZoneId.of(this.timezone))
            .toLocalDateTime();
    }
}
