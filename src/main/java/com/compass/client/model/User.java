package com.compass.client.model;

import com.compass.client.model.userhotelier.UserHotelier;

public class User {
	private UserHotelier userHotelier;

	public UserHotelier getUserHotelier() {
		return userHotelier;
	}

	public void setUserHotelier(UserHotelier userHotelier) {
		this.userHotelier = userHotelier;
	}
}
