package com.compass.client.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.ArrayList;

@JsonInclude(Include.NON_NULL)
public class RatePlanRoomUpdate {

    private Integer ratePlanId;
    private Integer contractRoomId;
    private String fromDate;
    private String toDate;
    private ArrayList<RateUpdate> rates = new ArrayList<>();

    public Integer getRatePlanId() {
        return ratePlanId;
    }

    public RatePlanRoomUpdate setRatePlanId(Integer ratePlanId) {
        this.ratePlanId = ratePlanId;

        return this;
    }

    public Integer getContractRoomId() {
        return contractRoomId;
    }

    public RatePlanRoomUpdate setContractRoomId(Integer contractRoomId) {
        this.contractRoomId = contractRoomId;

        return this;
    }

    public String getFromDate() {
        return fromDate;
    }

    public RatePlanRoomUpdate setFromDate(String fromDate) {
        this.fromDate = fromDate;

        return this;
    }

    public String getToDate() {
        return toDate;
    }

    public RatePlanRoomUpdate setToDate(String toDate) {
        this.toDate = toDate;

        return this;
    }

    public ArrayList<RateUpdate> getRates() {
        return rates;
    }

    public void addRates(RateUpdate rateUpdate) {
        this.rates.add(rateUpdate);
    }
}
