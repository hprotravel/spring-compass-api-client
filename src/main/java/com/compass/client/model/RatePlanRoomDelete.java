package com.compass.client.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class RatePlanRoomDelete {

    private Integer ratePlanId;
    private Integer contractRoomId;
    private String fromDate;
    private String toDate;
    private Integer minStay;
    private String paxOption;
    private Integer paxNumber;

    public Integer getRatePlanId() {
        return ratePlanId;
    }

    public RatePlanRoomDelete setRatePlanId(Integer ratePlanId) {
        this.ratePlanId = ratePlanId;

        return this;
    }

    public Integer getContractRoomId() {
        return contractRoomId;
    }

    public RatePlanRoomDelete setContractRoomId(Integer contractRoomId) {
        this.contractRoomId = contractRoomId;

        return this;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Integer getMinStay() {
        return minStay;
    }

    public void setMinStay(Integer minStay) {
        this.minStay = minStay;
    }

    public String getPaxOption() {
        return paxOption;
    }

    public RatePlanRoomDelete setPaxOption(String paxOption) {
        this.paxOption = paxOption;

        return this;
    }

    public Integer getPaxNumber() {
        return paxNumber;
    }

    public void setPaxNumber(Integer paxNumber) {
        this.paxNumber = paxNumber;
    }
}
