package com.compass.client.model.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse {

    private List<Error> errors;

    public List<Error> getErrors() {
        return errors;
    }

    public ErrorResponse setErrors(List<Error> errors) {
        this.errors = errors;

        return this;
    }
}
