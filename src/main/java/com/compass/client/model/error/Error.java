package com.compass.client.model.error;

public class Error {

    private String path;
    private String message;

    public String getPath() {
        return path;
    }

    public Error setPath(String path) {
        this.path = path;

        return this;
    }

    public String getMessage() {
        return message;
    }

    public Error setMessage(String message) {
        this.message = message;

        return this;
    }
}
