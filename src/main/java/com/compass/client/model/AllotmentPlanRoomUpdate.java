package com.compass.client.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AllotmentPlanRoomUpdate {

    private Integer allotmentPlanId;

    private Integer contractRoomId;
    private String fromDate;
    private String toDate;
    private Integer allotment;
    private String status;

    public Integer getAllotmentPlanId() {
        return allotmentPlanId;
    }

    public AllotmentPlanRoomUpdate setAllotmentPlanId(Integer allotmentPlanId) {
        this.allotmentPlanId = allotmentPlanId;

        return this;
    }

    public Integer getContractRoomId() {
        return contractRoomId;
    }

    public void setContractRoomId(Integer contractRoomId) {
        this.contractRoomId = contractRoomId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Integer getAllotment() {
        return allotment;
    }

    public void setAllotment(Integer allotment) {
        this.allotment = allotment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
