package com.compass.client.model;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class RatePlanRoomRate {
    private Integer id;
    private Date date;
    private HashMap<Integer, RateContent> rate;
    private String remark;
    private ContractRoom contractRoom;

    public Integer getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getDateInString() {
        return getDate().getDate();
    }

    public HashMap<Integer, RateContent> getRate() {
        return rate;
    }

    public String getRemark() {
        return remark;
    }

    public ContractRoom getContractRoom() {
        return contractRoom;
    }

    public Integer getContractRoomInId() {
        return getContractRoom().getId();
    }
}
