package com.compass.client.model.booking;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Date {

    protected String date;
    protected String timezone;

    @JsonProperty("timezone_type")
    protected Integer timezoneType;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimezone() {
        return timezone;
    }

    public Date setTimezone(String timezone) {
        this.timezone = timezone;

        return this;
    }

    public Integer getTimezoneType() {
        return timezoneType;
    }

    public Date setTimezoneType(Integer timezoneType) {
        this.timezoneType = timezoneType;

        return this;
    }
}
