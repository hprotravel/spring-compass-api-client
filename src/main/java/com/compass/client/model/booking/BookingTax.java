package com.compass.client.model.booking;

public class BookingTax {
    private Integer id;
    private Float amount;
    private String amountType;
    private String applyOption;
    private String rateOption;
    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    public String getApplyOption() {
        return applyOption;
    }

    public void setApplyOption(String applyOption) {
        this.applyOption = applyOption;
    }

    public String getRateOption() {
        return rateOption;
    }

    public BookingTax setRateOption(String rateOption) {
        this.rateOption = rateOption;

        return this;
    }

    public String getType() {
        return type;
    }

    public BookingTax setType(String type) {
        this.type = type;

        return this;
    }
}
