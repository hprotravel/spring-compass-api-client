package com.compass.client.model.booking;

public class BookingCancelledAt {
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
