package com.compass.client.model.booking;

public class BookingRoomDayPriceCreatedAt {

    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}