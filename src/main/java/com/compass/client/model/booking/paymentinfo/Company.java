package com.compass.client.model.booking.paymentinfo;

public class Company {

    private String commercialTitle;

    public String getCommercialTitle() {
        return commercialTitle;
    }

    public void setCommercialTitle(String commercialTitle) {
        this.commercialTitle = commercialTitle;
    }
}
