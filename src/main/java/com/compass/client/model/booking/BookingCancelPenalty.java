package com.compass.client.model.booking;

public class BookingCancelPenalty {

    private BookingPrice price;

    public BookingPrice getPrice() {
        return price;
    }

    public void setPrice(BookingPrice price) {
        this.price = price;
    }
}
