package com.compass.client.model.booking;

public class BookingUpdatedAt extends Date {

    public void setUpdatedAt(String date) {
        this.date = date;
    }
}
