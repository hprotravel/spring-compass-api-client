package com.compass.client.model;

import java.util.List;

public class Contract {

    private Integer id;
    private String name;
    private String status;
    private String type;
    private String code;
    private Integer childMaxPax;
    private String ratePlanType;
    private Date startDate;
    private Date endDate;
    private List<ContractRoom> contractRooms;
    private List<ContractUser> users;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public Contract setStatus(String status) {
        this.status = status;

        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getChildMaxPax() {
        return childMaxPax;
    }

    public void setChildMaxPax(Integer childMaxPax) {
        this.childMaxPax = childMaxPax;
    }

    public String getRatePlanType() {
        return ratePlanType;
    }

    public void setRatePlanType(String ratePlanType) {
        this.ratePlanType = ratePlanType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Contract setStartDate(Date startDate) {
        this.startDate = startDate;

        return this;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Contract setEndDate(Date endDate) {
        this.endDate = endDate;

        return this;
    }

    public List<ContractRoom> getContractRooms() {
        return contractRooms;
    }

    public void setContractRooms(List<ContractRoom> contractRooms) {
        this.contractRooms = contractRooms;
    }

    public List<ContractUser> getUsers() {
        return users;
    }

    public void setUsers(List<ContractUser> users) {
        this.users = users;
    }
}
