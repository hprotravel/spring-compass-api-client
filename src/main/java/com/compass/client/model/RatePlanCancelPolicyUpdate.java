package com.compass.client.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RatePlanCancelPolicyUpdate {
    private Integer contractRoomId;
    private String fromDate;
    private String toDate;
    private Integer days;
    private Double cancelAmount;
    private String cancelType;
    private String status;

    public Integer getContractRoomId() {
        return contractRoomId;
    }

    public RatePlanCancelPolicyUpdate setContractRoomId(Integer contractRoomId) {
        this.contractRoomId = contractRoomId;

        return this;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Double getCancelAmount() {
        return cancelAmount;
    }

    public void setCancelAmount(Double cancelAmount) {
        this.cancelAmount = cancelAmount;
    }

    public String getCancelType() {
        return cancelType;
    }

    public void setCancelType(String cancelType) {
        this.cancelType = cancelType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
