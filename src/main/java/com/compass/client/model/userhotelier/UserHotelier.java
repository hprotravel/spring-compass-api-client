package com.compass.client.model.userhotelier;

public class UserHotelier {
    private ChannelManager channelManager;
    private ChannelManagerEndpoint channelManagerEndpoint;

    public ChannelManager getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManager channelManager) {
        this.channelManager = channelManager;
    }

    public ChannelManagerEndpoint getChannelManagerEndpoint() {
        return channelManagerEndpoint;
    }

    public void setChannelManagerEndpoint(ChannelManagerEndpoint channelManagerEndpoint) {
        this.channelManagerEndpoint = channelManagerEndpoint;
    }
}
