package com.compass.client;

import com.compass.client.model.AllotmentPlanRestriction;
import com.compass.client.model.AllotmentPlanRestrictionUpdate;
import com.compass.client.model.AllotmentPlanRoom;
import com.compass.client.model.AllotmentPlanRoomUpdate;
import com.compass.client.model.Consumer;
import com.compass.client.model.Contract;
import com.compass.client.model.ContractInventoryPlan;
import com.compass.client.model.ContractList;
import com.compass.client.model.ContractRatePlan;
import com.compass.client.model.ContractRoom;
import com.compass.client.model.RatePlan;
import com.compass.client.model.RatePlanCancelPolicy;
import com.compass.client.model.RatePlanCancelPolicyUpdate;
import com.compass.client.model.RatePlanRoomDelete;
import com.compass.client.model.RatePlanRoomRate;
import com.compass.client.model.RatePlanRoomUpdate;
import com.compass.client.model.RatePlanSaleChannel;
import com.compass.client.model.RateUpdate;
import com.compass.client.model.User;
import com.compass.client.model.booking.Booking;
import com.compass.client.model.booking.ProductBooking;
import com.compass.client.model.booking.paymentinfo.PaymentInfo;
import java.util.HashMap;
import java.util.List;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.ResponseErrorHandler;

public interface CompassClient {

    void setResponseErrorHandler(ResponseErrorHandler errorHandler);

    List<ContractRatePlan> getContractsRatePlans(HashMap<String, String> headerMap, Integer contractId);

    List<AllotmentPlanRestriction> getAllotmentPlansRestrictionsListWithQuery(
            HashMap<String, String> headerMap,
            Integer allotmentPlanId,
            HashMap<String, String> queryMap
    );

    void createAllotmentPlansRestrictions(
            HashMap<String, String> headerMap,
            Integer allotmentPlanId,
            AllotmentPlanRestrictionUpdate allotmentPlanRestrictionUpdate);

  void bulkUpdateAllotmentPlansRestrictions(
      HashMap<String, String> headerMap,
      int allotmentPlanId,
      List<AllotmentPlanRestrictionUpdate> bulkUpdateList
  );

  void deleteAllotmentPlansRestrictions(
            HashMap<String, String> headerMap,
            Integer allotmentPlanId,
            AllotmentPlanRestrictionUpdate allotmentPlanRestrictionUpdate);

  void bulkDeleteAllotmentPlansRestrictions(
      HashMap<String, String> headerMap,
      int allotmentPlanId,
      List<AllotmentPlanRestrictionUpdate> bulkDeleteList
  );

  List<AllotmentPlanRestriction> getAllotmentPlansRestrictionsListWithQueryMultiRooms(
            HashMap<String, String> headerMap, Integer allotmentPlanId,
            HashMap<String, String> queryMap, List<Integer> contractRoomIds);

    List<AllotmentPlanRoom> getAllotmentPlansRoomsListWithQuery(
            HashMap<String, String> headerMap,
            Integer allotmentPlanId,
            HashMap<String, String> query
    );

    List<AllotmentPlanRoom> getAllotmentPlansRoomsListWithQueryMultiRooms(
            HashMap<String, String> headerMap,
            Integer allotmentPlanId,
            HashMap<String, String> query,
            List<Integer> contractRoomIds
    );

    // TODO: Remove it, because change method should be used instead of this.
    void createAllotmentPlansRooms(
            HashMap<String, String> headerMap,
            Integer allotmentPlanId,
            AllotmentPlanRoomUpdate allotmentPlanRoomUpdate
    );

    void changeAllotmentPlansRooms(
        HashMap<String, String> headerMap,
        Integer allotmentPlanId,
        AllotmentPlanRoomUpdate allotmentPlanRoomUpdate,
        HttpMethod requestType
    );

  void bulkUpdateAllotmentPlansRooms(
      HashMap<String, String> headerMap,
      int allotmentPlanId,
      List<AllotmentPlanRoomUpdate> bulkUpdateList
  );

  void bulkDeleteAllotmentPlansRooms(
      HashMap<String, String> headerMap,
      int allotmentPlanId,
      List<AllotmentPlanRoomUpdate> bulkDeleteList
  );

  ContractInventoryPlan getContractsInventoryPlans(HashMap<String, String> headerMap, Integer contractId);

    List<ContractRatePlan> getContractsRatePlansWithQuery(
            HashMap<String, String> headerMap,
            Integer contractId,
            HashMap<String, String> mapQuery
    );

    List<ContractRoom> getContractsRoomsList(HashMap<String, String> headerMap, Integer contractId);

    List<ContractRoom> getContractsRoomsListWithQuery(
            HashMap<String, String> headerMap,
            Integer contractId,
            HashMap<String, String> queryMap);

    Contract getContracts(HashMap<String, String> headerMap, Integer contractId);

    Contract getContractsWithQuery(
            HashMap<String, String> headerMap,
            Integer contractId,
            HashMap<String, String> queryMap);

    ContractList getContractsList(HashMap<String, String> headerMap);

    RatePlan getRatePlans(
            HashMap<String, String> headerMap,
            Integer ratePlanCode,
            HashMap<String, String> queryMap
    );

    ContractRoom getContractsRooms(
            HashMap<String, String> headerMap,
            Integer contractId,
            Integer roomCode
    );

    ProductBooking getProductsBookings(HashMap<String, String> headerMap);

    ProductBooking getProductsBookingsWithQuery(
            HashMap<String, String> headerMap,
            HashMap<String, String> queryMap);

    Booking getProductsBookingsWithId(
        HashMap<String, String> headerMap, Integer bookingId, HashMap<String, String> queryMap
    );

    PaymentInfo getProductsBookingsPaymentInfo(HashMap<String, String> headerMap, Integer bookingId);

    List<RatePlanRoomRate> getRatePlansRooms(
            HashMap<String,String> headerMap,
            Integer ratePlanId,
            HashMap<String, String> queryMap);

    void createRatePlansRooms(HashMap<String,String> headerMap, Integer ratePlanId, RateUpdate rateUpdate);

    void updateRatePlansRooms(
        HashMap<String,String> headerMap,
        Integer ratePlanId,
        Integer roomId,
        RatePlanRoomUpdate ratePlanRoomUpdate
    );

    void bulkUpdateRatePlansRooms(
        HashMap<String, String> headerMap,
        int ratePlanId,
        List<RatePlanRoomUpdate> bulkUpdateList
    );

  void deleteRatePlansRooms(HashMap<String,String> headerMap,Integer ratePlanId, RateUpdate rateUpdate);

  void bulkDeleteRatePlansRooms(
      HashMap<String, String> headerMap,
      int ratePlanId,
      List<RatePlanRoomDelete> bulkDeleteList
  );

  List<RatePlanCancelPolicy> getRatePlanCancelPoliciesWithQuery(HashMap<String,String> headerMap,Integer ratePlanId,
                                                                  HashMap<String, String> queryMap);

    void createRatePlansCancelPolicies(HashMap<String,String> headerMap,
                                       Integer ratePlanId,
                                       RatePlanCancelPolicyUpdate params);

    List<RatePlanCancelPolicy> getRatePlanCancelPoliciesWithQueryMultiRooms(HashMap<String, String> headerMap,
                                                                            Integer ratePlanId,
                                                                            HashMap<String, String> queryMap,
                                                                            List<Integer> contractRoomIds);

    List<RatePlanSaleChannel> getRatePlansSaleChannelsList(HashMap<String,String> headerMap, Integer ratePlanId);

    User getUsers(HashMap<String, String> headerMap, Integer userId, HashMap<String, String> queryMap);

    List<Consumer> getConsumersList(
        HashMap<String, String> headerMap, HashMap<String, String> queryMap
    );
}