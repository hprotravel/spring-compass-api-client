package com.compass.client.service;

import com.compass.client.model.AllotmentPlanRoom;
import com.compass.client.model.AllotmentPlanRoomUpdate;
import com.compass.client.util.HeaderService;
import com.compass.client.util.HelperEntityService;
import com.compass.client.util.RetryUtils;
import com.compass.client.util.ServiceProperties;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@EnableConfigurationProperties(ServiceProperties.class)
public class AllotmentPlanRoomService {

    private final ServiceProperties serviceProperties;
    private RestTemplate restTemplate;
    private HeaderService headerService;
    private HelperEntityService helperEntityService;

    public AllotmentPlanRoomService(ServiceProperties serviceProperties,
                                    RestTemplate restTemplate,
                                    HeaderService headerService,
                                    HelperEntityService helperEntityService) {
        this.serviceProperties = serviceProperties;
        this.restTemplate = restTemplate;
        this.headerService = headerService;
        this.helperEntityService = helperEntityService;
    }

    public List<AllotmentPlanRoom> getAllotmentPlansRoomsList(
            Map<String, String> headerMap,
            Integer allotmentPlanId,
            String query
            ){

        HttpEntity<String> entity = helperEntityService.httpEntity(headerMap);

        ResponseEntity<List<AllotmentPlanRoom>> allotmentPlanResponseEntity = restTemplate.exchange(
                serviceProperties.getUrl() + "/allotment-plans/" + allotmentPlanId +
                        "/rooms" + query,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<AllotmentPlanRoom>>() {
                });

        return allotmentPlanResponseEntity.getBody();
    }

    public List<AllotmentPlanRoom> getAllotmentPlansRoomsListMultiRooms(
            Map<String, String> headerMap,
            Integer allotmentPlanId,
            String query,
            List<Integer> contractRoomIds
    ) {
        StringBuilder queryBuilder = new StringBuilder(query);
        contractRoomIds.forEach(roomId -> queryBuilder.append("&contractRoomId[]=").append(roomId));
        query = queryBuilder.toString();

        return this.getAllotmentPlansRoomsList(headerMap, allotmentPlanId, query);
    }

    public void changeAllotmentPlansRooms(
        Map<String, String> headerMap,
        Integer allotmentPlanId,
        AllotmentPlanRoomUpdate allotmentPlanRoomUpdate,
        HttpMethod requestType
    ){
        HttpHeaders headers = headerService.httpHeaderService(headerMap);

        HttpEntity<AllotmentPlanRoomUpdate> allotmentPlanRoomUpdateHttpEntity = new HttpEntity<>(
            allotmentPlanRoomUpdate,
            headers
        );

        restTemplate.exchange(
            serviceProperties.getUrl() + "/allotment-plans/" + allotmentPlanId + "/rooms",
            requestType,
            allotmentPlanRoomUpdateHttpEntity,
            AllotmentPlanRoomUpdate.class
        );
    }

    public void bulkUpdate(
        Map<String, String> headerMap,
        int allotmentPlanId,
        List<AllotmentPlanRoomUpdate> bulkUpdateList
    ){
        retryBulkRequest(
            String.format("%s/allotment-plans/%d/rooms/bulk", serviceProperties.getUrl(), allotmentPlanId),
            HttpMethod.POST,
            new HttpEntity<>(
                Collections.singletonMap("requests", bulkUpdateList),
                headerService.httpHeaderService(headerMap)
            )
        );
    }

    public void bulkDelete(
        Map<String, String> headerMap,
        int allotmentPlanId,
        List<AllotmentPlanRoomUpdate> bulkDeleteList
    ){
        retryBulkRequest(
            String.format("%s/allotment-plans/%d/rooms/bulk", serviceProperties.getUrl(), allotmentPlanId),
            HttpMethod.DELETE,
            new HttpEntity<>(
                Collections.singletonMap("requests", bulkDeleteList),
                headerService.httpHeaderService(headerMap)
            )
        );
    }

    private void retryBulkRequest(String url, HttpMethod method, HttpEntity headerEntity) {
        RetryUtils.retryBulkRequest(url, method, headerEntity, restTemplate);
    }
}
