package com.compass.client.service;

import com.compass.client.model.RatePlanRoomRate;
import com.compass.client.model.RateUpdate;
import com.compass.client.util.HeaderService;
import com.compass.client.util.HelperEntityService;
import com.compass.client.util.ServiceProperties;
import java.util.List;
import java.util.Map;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

@EnableConfigurationProperties(ServiceProperties.class)
public class RatePlanRoomListService {

    private final ServiceProperties serviceProperties;
    private RestTemplate restTemplate;
    private HeaderService headerService;
    private HelperEntityService helperEntityService;

    public RatePlanRoomListService(ServiceProperties serviceProperties, RestTemplate restTemplate,
                           HeaderService headerService, HelperEntityService helperEntityService) {
        this.serviceProperties = serviceProperties;
        this.restTemplate = restTemplate;
        this.headerService = headerService;
        this.helperEntityService = helperEntityService;
    }

    public void createRatePlansRooms(Map<String, String> headerMap, Integer ratePlanId, RateUpdate params) {
        HttpHeaders httpHeaders = headerService.httpHeaderService(headerMap);
        HttpEntity<RateUpdate> entity = new HttpEntity<>(params, httpHeaders);
        restTemplate.exchange(serviceProperties.getUrl() + "/rate-plans/" + ratePlanId + "/rooms",
                HttpMethod.POST, entity, RateUpdate.class);
    }

    public List<RatePlanRoomRate> getRatePlansRooms(Map<String, String> headerMap, Integer ratePlanId, String query) {
        return restTemplate.exchange(
            serviceProperties.getUrl() + "/rate-plans/" + ratePlanId + "/rooms" + query,
            HttpMethod.GET,
            helperEntityService.httpEntity(headerMap),
            new ParameterizedTypeReference<List<RatePlanRoomRate>>() {}
        ).getBody();
    }

    public void deleteRatePlansRooms(Map<String, String> headersMap, Integer ratePlanId, RateUpdate params) {
        HttpHeaders httpHeaders = headerService.httpHeaderService(headersMap);
        HttpEntity<RateUpdate> entity = new HttpEntity<>(params, httpHeaders);
        restTemplate.exchange(
                serviceProperties.getUrl() + "/rate-plans/" + ratePlanId + "/rooms",
                HttpMethod.DELETE, entity, RateUpdate.class);
    }
}
