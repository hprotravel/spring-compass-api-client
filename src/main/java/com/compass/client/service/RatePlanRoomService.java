package com.compass.client.service;

import com.compass.client.model.RatePlanRoomDelete;
import com.compass.client.model.RatePlanRoomUpdate;
import com.compass.client.util.HeaderService;
import com.compass.client.util.HelperEntityService;
import com.compass.client.util.RetryUtils;
import com.compass.client.util.ServiceProperties;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

@EnableConfigurationProperties(ServiceProperties.class)
public class RatePlanRoomService {

    private final ServiceProperties serviceProperties;
    private RestTemplate restTemplate;
    private HeaderService headerService;
    private HelperEntityService helperEntityService;

    public RatePlanRoomService(ServiceProperties serviceProperties,
                               RestTemplate restTemplate,
                               HeaderService headerService,
                               HelperEntityService helperEntityService) {
        this.serviceProperties = serviceProperties;
        this.restTemplate = restTemplate;
        this.headerService = headerService;
        this.helperEntityService = helperEntityService;
    }

    public void createRatePlansRooms(
            Map<String, String> headerMap,
            Integer ratePlanId,
            Integer roomId,
            RatePlanRoomUpdate ratePlanRoomUpdate
    ){
        HttpHeaders headers = headerService.httpHeaderService(headerMap);

        HttpEntity<RatePlanRoomUpdate> ratePlanRoomUpdateHttpEntity =
                new HttpEntity<>(ratePlanRoomUpdate, headers);

        restTemplate.exchange(serviceProperties.getUrl() + "/rate-plans/" +
                        ratePlanId
                        + "/rooms/" + roomId,
                HttpMethod.POST,
                ratePlanRoomUpdateHttpEntity,
                RatePlanRoomUpdate.class
        );
    }

    public void updateRatePlansRooms(
        Map<String, String> headerMap,
        Integer ratePlanId,
        Integer roomId,
        RatePlanRoomUpdate ratePlanRoomUpdate
    ) {
        HttpHeaders headers = headerService.httpHeaderService(headerMap);

        restTemplate.exchange(
            serviceProperties.getUrl() + "/rate-plans/" + ratePlanId + "/rooms/" + roomId,
            HttpMethod.PUT,
            new HttpEntity<>(ratePlanRoomUpdate, headers),
            RatePlanRoomUpdate.class
        );
    }

    public void bulkUpdate(
        Map<String, String> headerMap,
        int ratePlanId,
        List<RatePlanRoomUpdate> bulkUpdateList
    ) {
        retryBulkRequest(
            String.format("%s/rate-plans/%d/rooms/bulk", serviceProperties.getUrl(), ratePlanId),
            HttpMethod.POST,
            new HttpEntity<>(
                Collections.singletonMap("requests", bulkUpdateList),
                headerService.httpHeaderService(headerMap)
            )
        );
    }

    public void deleteRatePlansRooms(
        Map<String, String> headerMap, Integer ratePlanId, Integer roomId, RatePlanRoomDelete ratePlanRoomDelete
    ) {
        restTemplate.exchange(
            serviceProperties.getUrl() + "/rate-plans/" + ratePlanId + "/rooms/" + roomId,
            HttpMethod.DELETE,
            new HttpEntity<>(ratePlanRoomDelete, headerService.httpHeaderService(headerMap)),
            RatePlanRoomDelete.class
        );
    }

    public void bulkDelete(
        Map<String, String> headerMap,
        int ratePlanId,
        List<RatePlanRoomDelete> bulkDeleteList
    ) {
        retryBulkRequest(
            String.format("%s/rate-plans/%d/rooms/bulk", serviceProperties.getUrl(), ratePlanId),
            HttpMethod.DELETE,
            new HttpEntity<>(
                Collections.singletonMap("requests", bulkDeleteList),
                headerService.httpHeaderService(headerMap)
            )
        );
    }

    private void retryBulkRequest(String url, HttpMethod method, HttpEntity headerEntity) {
        RetryUtils.retryBulkRequest(url, method, headerEntity, restTemplate);
    }
}
