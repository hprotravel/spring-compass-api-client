# Spring Boot Compass API Client

Channel Manager Java client for Compass Core API

### Summary

It is a package used by Compass microservices and manages REST communication between Service and API.

### Usage

You should add JitPack repository for importing dependency:

```
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```

Then you can just add Maven dependency like this:

 ```
 <dependencies>
     ..............
     <!-- Compass API client package !-->
     <dependency>
 	    <groupId>org.bitbucket.hprotravel</groupId>
 	    <artifactId>spring-compass-api-client</artifactId>
     </dependency>
     ..............
 </dependencies>
 ```

You can switch logging on and off by setting `compass.request-logging` property to `true` or `false`.